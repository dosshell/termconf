#!/bin/bash

rm -f ~/.vimrc
rm -rf ~/.local/share/nvim/
rm -rf ~/.local/state/nvim/
rm -rf ~/.cache/nvim/
rm -f ~/.local/bin/nvim
rm -f ~/.config/nvim

sudo apt install vim wget fd-find pip python3-neovim luarocks -y
mkdir -p ~/.local/bin/
wget -O ~/.local/bin/nvim https://github.com/neovim/neovim/releases/download/v0.10.2/nvim.appimage
chmod u+x ~/.local/bin/nvim

ln -s ~/.config/termconf/config-vim ~/.vimrc
ln -s ~/.config/termconf/lazyvim ~/.config/nvim

nvim
