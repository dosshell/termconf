#!/bin/bash

TCPATH="$(dirname "$(readlink -f "$0")")"
mkdir -p ~/.config
ln -s $TCPATH ~/.config/termconf

sudo apt install i3

mkdir -p ~/.config/i3/
ln -si  ~/.config/termconf/config-i3 ~/.config/i3/config
ln -si  ~/.config/termconf/lock_img.png ~/.config/i3/lock_img.png

mkdir -p ~/.config/i3status/
ln -si  ~/.config/termconf/config-i3status ~/.config/i3status/config
