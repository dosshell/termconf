# Install
Note that `sudo` should not be used.

```
git clone https://gitlab.com/dosshell/termconf.git --recursive
cd termconf
git remote set-url origin git@gitlab.com:/dosshell/termconf.git
./install-term.sh
```

Windows Terminal:
* Set default profile in UI (Startup->Default profile)
* Modify settings.json:
```
...
"defaults":
{
    // Put settings here that you want to apply to all profiles.
    "fontFace": "Lucida Console",
    "fontSize": 10,
    "antialiasingMode": "cleartype",
    "colorScheme": "One Half Dark"
},
...
"actions":
[
    { "command": null, "keys": "ctrl+alt+1" },
    { "command": null, "keys": "ctrl+alt+2" },
    { "command": null, "keys": "ctrl+alt+3" },
    { "command": null, "keys": "ctrl+alt+4" },
    { "command": null, "keys": "ctrl+alt+5" },
    { "command": null, "keys": "ctrl+alt+6" },
    { "command": null, "keys": "ctrl+alt+7" },
    { "command": null, "keys": "ctrl+alt+8" },
    { "command": null, "keys": "ctrl+alt+9" }
]
...
```

# Templates
```
new cpp myproject
```
