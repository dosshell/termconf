#!/bin/bash

DISPLAY_COUNT=$(xrandr | grep " connected " | wc -l)
echo Found $DISPLAY_COUNT display\(s\).

if [ "$DISPLAY_COUNT" -eq "1" ]; then
    echo "Using external monitor only."
    xrandr --output eDP-1-1 --auto --output DP-1-1 --off --output DP-1-2 --off --output DP-1-3 --off --output DP-1-1-1 --off --output DP-1-1-2 --off --output DP-1-1-3 --off
fi

if [ "$DISPLAY_COUNT" -eq "2" ]; then
    echo "Using external monitor only."
    xrandr --output eDP-1-1 --auto --output DP-1-1 --auto --right-of eDP-1-1 --output DP-1-2 --auto --right-of eDP-1-1 --output DP-1-3 --auto --right-of eDP-1-1 --primary --dpi 109
    i3-msg restart
fi

if [ "$DISPLAY_COUNT" -eq "3" ]; then
    echo "Using external monitor only."
    xrandr --output eDP-1-1 --off --output DP-1-1 --off --output DP-1-2 --off --output DP-1-3 --off  --output DP-1-1-1 --auto --output DP-1-1-2 --auto --right-of DP-1-1-1 --dpi 109
    i3-msg restart
fi
