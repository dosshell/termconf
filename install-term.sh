#!/bin/bash

TCPATH="$(dirname "$(readlink -f "$0")")"
mkdir -p ~/.config
ln -s $TCPATH ~/.config/termconf

sudo apt install zsh tig vim git-lfs ripgrep tmux pipx pip fd-find python-is-python3 build-essential -y
sudo pipx install poetry

ssh-keygen -t ed25519 -a 100

source setup-python.sh
source setup-zsh.sh
source setup-vim.sh
source setup-git.sh
source setup-tig.sh
source setup-fzf.sh

