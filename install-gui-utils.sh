#!/bin/bash

TCPATH="$(dirname "$(readlink -f "$0")")"
mkdir -p ~/.config
ln -s $TCPATH ~/.config/termconf

sudo apt install software-properties-common apt-transport-https wget -y

wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" -y

sudo usermod -a -G video $(whoami)
sudo usermod -a -G input $(whoami)

sudo apt install code terminator pavucontrol
