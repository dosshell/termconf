#!/bin/bash

TCPATH="$(dirname "$(readlink -f "$0")")"
ln -s $TCPATH/fonts ~/.fonts

echo 'Use "UbuntuSansMonoNerdFont" as terminal font to use neovim and powerline etc'
