#!/bin/bash

rm ~/.vimrc
rm ~/.zshrc
rm ~/.config/termconf
rm /usr/bin/new
rm ~/.config/i3/config
rm ~/.config/i3/lock_img.png
rmdir ~/.config/i3
rm ~/.config/i3status/config
rmdir ~/.config/i3status
