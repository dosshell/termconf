bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word

bindkey '\e[3~' delete-char
bindkey '\e[2~' quoted-insert
