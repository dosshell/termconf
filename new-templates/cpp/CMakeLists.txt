cmake_minimum_required(VERSION 3.10)
project(someproject)

set(CMAKE_CXX_STANDARD 17)

# Add warning levels for gcc and msvc
if (MSVC)
  if (CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
    string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
  else ()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
  endif ()
  if (CMAKE_C_FLAGS MATCHES "/W[0-4]")
    string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_C_FLAGS "${CMAKE_C_FLAGS}")
  else ()
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /W4")
  endif ()
elseif (CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
  set(COMMON_GXX_FLAGS "-Wall -Wextra -pedantic -Winit-self -Wpointer-arith -Wcast-align -Wconversion -Wshadow -Wundef -Wwrite-strings -Wswitch-enum -Wswitch-default")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${COMMON_GXX_FLAGS} -Wstrict-prototypes -Wmissing-prototypes")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMMON_GXX_FLAGS}")
  add_definitions(-D_DEFAULT_SOURCE)
endif ()

if (WIN32)
    set(WINVER 0x0600) # Windows Vista
    set(_WIN32_WINNT 0x0600)
endif()

option(ENABLE_TESTS "Enable tests" ON)
option(WARNINGS_AS_ERRORS "Enable treat warnings as errors" OFF)

add_executable(someproject ${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp)
target_compile_definitions(someproject PUBLIC DOCTEST_CONFIG_DISABLE)
if (ENABLE_TESTS)
  add_executable(someproject_tests ${CMAKE_CURRENT_SOURCE_DIR}/src/tests.cpp)
endif ()
