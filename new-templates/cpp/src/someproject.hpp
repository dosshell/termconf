#include "doctest.h"

inline int foo();

TEST_CASE("some foo test")
{
    CHECK(foo() == 0);
}

inline int foo()
{
    return 0;
}
