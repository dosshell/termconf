#!/bin/bash

current_username=$(git config user.name)
current_email=$(git config user.email)

read -p "name [$current_username]: " varname
read -p "email [$current_email]: " varemail

varname=${varname:-$current_username}
varemail=${varemail:-$current_email}

git config --global user.name "$varname"
git config --global user.email "$varemail"

git config --global core.editor vim
git config --global push.default current

git config --global rerere.enabled 1

git config --global core.autocrlf false
git config --global core.fscache true

git config --global merge.tool vscode
git config --global mergetool.vscode.cmd "code --wait $MERGED"
git config --global diff.tool vscode
git config --global difftool.vscode.cmd "code --wait --diff $LOCAL $REMOTE"

git config --global mergetool.keepBackup false
#git config --global merge.conflictstyle diff

git lfs install --skip-repo
