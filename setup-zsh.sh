#!/bin/bash

rm ~/.zshrc
ln -s ~/.config/termconf/config-zsh ~/.zshrc

which zsh
if [ $? -eq 0 ]; then
    ZSH_PATH="$(which zsh)"
    chsh -s $ZSH_PATH
fi

sudo ln -sf ~/.config/termconf/new.sh /usr/bin/new

DEVENVPATH=$(find /mnt/c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio/ -maxdepth 5 -name devenv.exe | head -n 1)
sudo ln -sf "$DEVENVPATH" "/usr/bin/vs"
